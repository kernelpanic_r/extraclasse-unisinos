package extraclass;

 

public class Robo {

    protected int linha;
    private int linhaFinal;
    protected int coluna;
    private int colunaFinal;
    protected int lInicial, cInicial;
    private int movimentos;
    
    public Robo(int linha, int coluna, int linhaFinal, int colunaFinal) {
        this.linha = linha;
        this.linhaFinal = linhaFinal;
        this.coluna = coluna;
        this.colunaFinal = colunaFinal;
        lInicial = linha; 
        cInicial = coluna;
    }
    
    public Robo(int vetor[]) {
        linha = vetor[0];
        coluna = vetor[1];
        linhaFinal = vetor[2];
        colunaFinal = vetor[3];
        lInicial = linha;
        cInicial = coluna;      
    }

    public int getLinha() {
        return linha;
    }

    public int getLinhaFinal() {
        return linhaFinal;
    }

    public int getColuna() {
        return coluna;
    }

    public int getColunaFinal() {
        return colunaFinal;
    }

    public void andarParaCima() {
        linha += 1;
        ++movimentos;
    }
    
   
    public void andarParaBaixo() {
        linha -= 1;
        ++movimentos;
    }
    
    public void andarParaDireita() {
        coluna += 1;
        ++movimentos;
    }

    public void andarParaEsquerda() {
        coluna -= 1;
        ++movimentos;
    }

    public boolean chegouLinha() {
        return linha == getLinhaFinal();
    }

    public boolean chegouColuna() {
        return coluna == getColunaFinal();
    }

    public boolean chegouDestino() {
        return chegouLinha() && chegouColuna();
    }
    
    public int getMovimentos() {
    	return movimentos;
    }
    
    public String roboInfo(Robo robo) {
        String inicio = "Inicio em: Linha: " + robo.lInicial + " Coluna: "+ robo.cInicial;
        String termino = "Fim em: Linha: "+ robo.getLinhaFinal() + " Coluna " + robo.getColunaFinal();
        return "O Robo: " + robo.getClass().getSimpleName() + " chegou com " + movimentos + " movimentos\n"
                + inicio + "\n" + termino;
    }
    
    public String roboStart(Robo robo) {
        String destino = "\nCom destino para: Linha:" + robo.linhaFinal + " Coluna: " + robo.colunaFinal;
        return robo.getClass().getSimpleName() + " Iniciou em: Linha: " + robo.lInicial + " Coluna: " + robo.cInicial + destino;
        
    }

}


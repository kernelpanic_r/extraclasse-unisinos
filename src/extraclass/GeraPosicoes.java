package extraclass;

public final class GeraPosicoes {

	private static int geraValor(int valorMaximo) {
		return (int) (Math.random() * valorMaximo);
	}

	public static int[] geraPosicao(Terreno terreno) {
		int vetor[] = new int[4];
		for (int i = 0; i < vetor.length; i++)
			vetor[i] = geraValor(terreno.max());
		return vetor;
	}

	public static Robo geraRobo(Terreno terreno, boolean terrestre) {
		if (terrestre) {
			int[] vetor ;
			while (true) {
				vetor = geraPosicao(terreno);
				if (!terreno.isAlagada(vetor[0], vetor[1]) && !terreno.isAlagada(vetor[2], vetor[3])) {
					return new RoboTerrestre(vetor);
				}
			}
		}
		return new RoboBarco(geraPosicao(terreno));
	}
	
	public static Robo[] geraRobos(Terreno terreno, boolean terrestre, int numero) {
		Robo robos[];
		if (terrestre) {
			robos = new RoboTerrestre[numero];
			for (int i = 0; i < numero; i++) {
				robos[i] = geraRobo(terreno, true);
			}
		} else {
			robos = new RoboBarco[numero];
			for (int i = 0; i < numero; i++) {
				robos[i] = geraRobo(terreno, false);
			}
		}
		return robos;
	}

}

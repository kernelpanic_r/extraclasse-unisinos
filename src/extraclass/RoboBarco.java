package extraclass;

public class RoboBarco extends Robo {

	public RoboBarco(int linha, int coluna, int linhaFinal, int colunaFinal) {
		super(linha, coluna, linhaFinal, colunaFinal);
	}
	
	public RoboBarco(int vetor[]) {
		super(vetor);
	}
}

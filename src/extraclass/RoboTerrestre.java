package extraclass;

public class RoboTerrestre extends Robo {

	public RoboTerrestre(int linha, int coluna, int linhaFinal, int colunaFinal) {
		super(linha, coluna, linhaFinal, colunaFinal);
	}
	
	public RoboTerrestre(int vetor[]) {
		super(vetor);
	}
	
	public String naoPodeAndar() {
		String inicio = "Linha inicial: " + lInicial + " Coluna inicial: " + cInicial;
		String destino = "\nLinha final: " + getLinhaFinal() + " Coluna final: " + getColunaFinal();
		return "Não pode andar, pois a área está alagada!\n" + inicio + destino;
	}
	
	
}

package extraclass;

public final class Terreno {

    private String areaAlagada[][];
    private char roboPadrao;
    private char destinoPadrao;
    
    
    public Terreno(char robo, char destino) {
    	this.roboPadrao = robo;
    	this.destinoPadrao = destino;
        areaAlagada = new String[50][50];
        preencheCampo();
    }
    
    public Terreno() {
    	roboPadrao = 'R';
    	destinoPadrao = 'D';
    	areaAlagada = new String[50][50];
        preencheCampo();
    	
    }

    public void preencheCampo() {
    	marcaCampos();
    	marcacoes();
    }
    
    private void marcaCampos(){
        for(int i = 0; i < areaAlagada.length; i++)
            for(int z =0; z < 50; z++) areaAlagada[i][z] = "(.)";
    }
    
    private void marcacoes() {
        primeiroTrecho();
        segundoTrecho();
        terceiroTrecho();
        quartoTrecho();
    }

    public int max() {
        return areaAlagada.length;
    }

    public boolean isAlagada(int linha, int coluna) {
        return areaAlagada[linha][coluna].equals("(X)");
    }

    private void marcaAreas(int linha, int maximo, int coluna) {
        for (int i = 0; i < maximo; i++) {
            areaAlagada[linha][coluna++] = "(X)";
        }
    }

    private void primeiroTrecho() {
        for (int i = 3; i < 9; i++) {
            marcaAreas(i, 13, 6);
        }
    }

    private void segundoTrecho() {
        for (int i = 2; i < 20; i++) {
            marcaAreas(i, 5, 39);
        }
    }

    private void terceiroTrecho() {
        for (int i = 12; i < 19; i++) {
            marcaAreas(i, 17, 18);
        }
    }

    private void quartoTrecho() {
        for (int i = 26; i < 34; i++) {
            marcaAreas(i, 21, 5);
        }
    }

    public String showTerreno() {
        StringBuilder terreno = new StringBuilder();
        for(int i = 0; i < areaAlagada.length; i++){
            for(int z = 0; z < 50; z++){
                terreno.append(areaAlagada[i][z]);
            }
            terreno.append("\n");
        }
        return terreno.toString();
    }
    
    public int fimAreaAlagadaColuna(boolean direita, int linha, int coluna  ) {
    	int distancia = 0;
    	if(direita) {	
    		for(;coluna < 50; coluna++) {
    			++distancia;
    			if(!areaAlagada[linha][coluna].equals("(X)")) return distancia - 1 ;    		
    		}
    	}else {
    		for(;0 < coluna; coluna--) {
    			++distancia;
    			if(!areaAlagada[linha][coluna].equals("(X)")) return distancia - 1;
    		}
    	}
    	return distancia;
    }
    
    public int fimAreaAlagadaLinha(boolean cima, int linha, int coluna ) {
    	int distancia = 0;
    	if(cima) {
    		for(;linha < 50; linha++) {
    			++distancia;
    			if(!areaAlagada[linha][coluna].equals("(X)")) return distancia - 1;
    		}
    	}else {
    		for(;0 <  linha; linha --) {
    			++distancia;
    			if(!areaAlagada[linha][coluna].equals("(X)")) return distancia - 1;
    		}
    	}
    	return distancia;
    }
    

    public boolean podeAndar(int linha, int coluna, Robo robo) {
        if (linha >= 0 & coluna >= 0 && linha < areaAlagada.length && coluna < 50) {
            if (robo instanceof RoboBarco)
                return true;
            return !isAlagada(linha, coluna);
        }
        return false;
    }
    
   
    
    public void altera(int linha, int coluna){
        areaAlagada[linha][coluna] = " " + roboPadrao + " ";
    }
    
    public void setaDestino(int linha, int coluna) {
    	areaAlagada[linha][coluna] = " " + destinoPadrao + " ";
    }
    
 
    
}


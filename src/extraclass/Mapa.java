package extraclass;

public final class Mapa {

	private Terreno terreno;

	public Mapa(Terreno terreno) {
		this.terreno = terreno;
	}

	private boolean cima(Robo robo) {
		return terreno.podeAndar(robo.getLinha() + 1, robo.getColuna(), robo);
	}

	private boolean baixo(Robo robo) {
		return terreno.podeAndar(robo.getLinha() - 1, robo.getColuna(), robo);
	}

	private boolean direita(Robo robo) {
		return terreno.podeAndar(robo.getLinha(), robo.getColuna() + 1, robo);
	}

	private boolean esquerda(Robo robo) {
		return terreno.podeAndar(robo.getLinha(), robo.getColuna() - 1, robo);
	}

	private void cimaOuBaixo(Robo robo) {
		if (robo.getLinha() < robo.getLinhaFinal()) {
			if (cima(robo)) {
				robo.andarParaCima();
				terreno.altera(robo.getLinha(), robo.getColuna());
			}
		} else if (robo.getLinha() > robo.getLinhaFinal()) {
			if (baixo(robo)) {
				robo.andarParaBaixo();
				terreno.altera(robo.getLinha(), robo.getColuna());
			}
		}
	}

	private void direitaOuEsquerda(Robo robo) {
		if (robo.getColuna() < robo.getColunaFinal()) {
			if (direita(robo)) {
				robo.andarParaDireita();
				terreno.altera(robo.getLinha(), robo.getColuna());
			}
		} else if (robo.getColuna() > robo.getColunaFinal()) {
			if (esquerda(robo)) {
				robo.andarParaEsquerda();
				terreno.altera(robo.getLinha(), robo.getColuna());
			}
		}
	}

	public void andar(Robo robo) throws Exception {

		terreno.preencheCampo();

		System.out.println("Iniciando o " + robo.getClass().getSimpleName());
		System.out.println("Posi��o inicial: Linha:" + robo.getLinha() + " Coluna: " + robo.getColuna());
		System.out.println("Posi��o de destino: Linha: " + robo.getLinhaFinal() + " Coluna: " + robo.getColunaFinal());
		Thread.sleep(1000);

		terreno.altera(robo.getLinha(), robo.getColuna());
		terreno.setaDestino(robo.getLinhaFinal(), robo.getColunaFinal());

		System.out.println(terreno.showTerreno());

		int i = 0;
		int linha = 0, coluna = 0;
		while (!robo.chegouDestino()) {

			linha = robo.getLinha();
			coluna = robo.getColuna();

			cimaOuBaixo(robo);
			if (linha == robo.getLinha()) {
				direitaOuEsquerda(robo);
			}
			
			if (linha == robo.getLinha() && coluna == robo.getColuna()) {

				if (robo.chegouLinha()) {
					int colunaAuxiliar = 0;

					if (coluna < robo.getColunaFinal()) {
						colunaAuxiliar = coluna + 1;
					} else {
						colunaAuxiliar = coluna - 1;
					}

					int linhaCima = terreno.fimAreaAlagadaLinha(true, robo.getLinha(), colunaAuxiliar);
					int linhaBaixo = terreno.fimAreaAlagadaLinha(false, robo.getLinha(), colunaAuxiliar);

					if (linhaCima < linhaBaixo) {
						for (int z = 0; z < linhaCima; z++) {
							robo.andarParaCima();
							terreno.altera(robo.getLinha(), robo.getColuna());
						}
					} else {
						for (int z = 0; z < linhaBaixo; z++) {
							robo.andarParaBaixo();
							terreno.altera(robo.getLinha(), robo.getColuna());
						}

					}
					direitaOuEsquerda(robo);
				}
				if (robo.chegouColuna()) {

					int linhaAuxiliar = 0;
					if (linha < robo.getLinhaFinal()) {
						linhaAuxiliar = linha + 1;
					} else {
						linhaAuxiliar = linha - 1;
					}

					int colunaDireita = terreno.fimAreaAlagadaColuna(true, linhaAuxiliar, robo.getColuna());
					int colunaEsquerda = terreno.fimAreaAlagadaColuna(false, linhaAuxiliar, robo.getColuna());
					if (colunaDireita < colunaEsquerda) {
						for (int z = 0; z < colunaDireita; z++) {
							robo.andarParaDireita();
							terreno.altera(robo.getLinha(), robo.getColuna());
						}
					} else {
						for (int z = 0; z < colunaEsquerda; z++) {
							robo.andarParaEsquerda();
							terreno.altera(robo.getLinha(), robo.getColuna());
						}
					}
					cimaOuBaixo(robo);
				}
			}

			if (i++ == 100) {
				System.out.println("Loop");
				System.out.println(robo.getLinha() + "  " + robo.getColuna());
				System.out.println(terreno.showTerreno());
				System.exit(0);
			}
		}

		System.out.println(terreno.showTerreno());
		System.out.println(robo.roboInfo(robo));
		Thread.sleep(1000);
	}

}

package extraclass;

public final class Navegar {

	public static void main(String a[]) throws Exception {

	
		Terreno terreno = new Terreno('M', 'K');
		Mapa mapa = new Mapa(terreno);
	
		Robo robos [] = GeraPosicoes.geraRobos(terreno, true, 10);
		for(int i = 0; i < robos.length; i++) {
			mapa.andar(robos[i]);
		}
		
	}

}
